<?php

/*
 * RSS2 closure feed template for displaying closure_feed.
 */
header('Content-Type: ' . feed_content_type('rss2') . '; charset=' . get_option('blog_charset'), true);
$frequency  = 2; /* Default '1'. The frequency of RSS updates within the update period. */
$duration   = 'daily'; /* Default 'hourly'. Accepts 'hourly', 'daily', 'weekly', 'monthly', 'yearly'. */

echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>';

/**
 * Fires between the xml and rss tags in a feed.
 *
 * @since 4.0.0
 *
 * @param string $context Type of feed. Possible values include 'rss2', 'rss2-comments',
 *                        'rdf', 'atom', and 'atom-comments'.
 */
do_action('rss_tag_pre', 'rss2');

$feed = $args['feed'];
$feed_count = min($args['feed_count'], 20);
$feed_update = $args['feed_update'];
$options = get_option('routenbuch_options');
?>

<rss version="2.0"
     xmlns:content="http://purl.org/rss/1.0/modules/content/"
     xmlns:wfw="http://wellformedweb.org/CommentAPI/"
     xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:atom="http://www.w3.org/2005/Atom"
     xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
     xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
<?php
/**
 * Fires at the end of the RSS root to add namespaces.
 *
 * @since 2.0.0
 */
do_action('rss2_ns');
?>
>
    <channel>
        <title>Aktuelle Sperrungen - <?php echo bloginfo_rss('title') ?></title>
        <link><?php bloginfo_rss('url') ?></link>
        <description>Vogelschutz- und Naturschutzsperrungen für das Nördliche und Südliche Frankenjura</description>
        <lastBuildDate><?php echo date('D, d M Y H:i:s +0000', $feed_update); ?></lastBuildDate>
        <language><?php bloginfo_rss('language'); ?></language>
        <sy:updatePeriod><?php echo apply_filters('rss_update_period', $duration); ?></sy:updatePeriod>
        <sy:updateFrequency><?php echo apply_filters('rss_update_frequency', $frequency); ?></sy:updateFrequency>
        <atom:link href="<?php self_link(); ?>" rel="self" type="application/rss+xml" />
        <?php do_action('rss2_head'); ?>

        <!-- Start item loop -->
<?php
for($i = 0; $i < $feed_count; $i++) {
    $element = $feed[$i];
    $attr = $element['attributes'];

    $feed_time = strtotime($element['meta']['updated_at']);

    $attr_reason = $attr['reason'];
    if ($attr['reason'] == 'breeding birds') {
        $attr_reason = __('breeding birds closure', 'routenbuch');
    } else if ($attr['reason'] == 'bats') {
        $attr_reason = __('bats closure', 'routenbuch');
    } else if ($attr['reason'] == 'protected plants') {
        $attr_reason = __('protected plants closure', 'routenbuch');
    } else if ($attr['reason'] == 'administrative closure') {
        $attr_reason = __('administrative closure', 'routenbuch');
    } else if ($attr['reason'] == 'legal regulations') {
        $attr_reason = __('legal regulations closure', 'routenbuch');
    }

    if ($attr['active']) {
        $feed_title = '🔒 ' . $attr['name'] . ' (' . $attr['region'][0] . ') - ' . $attr_reason . ' ' . __('from', 'routenbuch') . ' ' . $attr['start_at'] . ' ' . __('to', 'routenbuch') . ' ' . $attr['end_at'];
    } else {
        $feed_title = '🔓 ' . $attr['name'] . ' (' . $attr['region'][0] . ') - ' . $attr_reason . ' ' . __('was lifted', 'routenbuch');
    }
    $feed_guid = hash('sha1', $attr['name'] . $attr['region'][0] . $attr['start_at'] . $attr['end_at']);
    $feed_guid_url = get_site_url() . $options['routenbuch_list_page'] . '#' . $feed_guid;

    $feed_desc = '';
    if (strlen($attr['season_description']) > 0) {
        $feed_desc .= '🔔 ' . $attr['season_description'] . '<br />';
    }

    if (strlen($attr['description']) > 0) {
        $feed_desc .= '📝 ' . $attr['description'] . '<br />';
    }

    if ($attr['active']) {
        $geo_ref_count = count($attr['geo_refs']);
        for ($j = 0; $j < $geo_ref_count; $j++) {
            $geo_ref = $attr['geo_refs'][$j];

            $feed_desc .= '🏔 ' . $geo_ref['name'];
            if ($geo_ref['type'] === "Sector" &&
                strlen($geo_ref['route_first']) > 0 &&
                strlen($geo_ref['route_last']) > 0) {
                $feed_desc .= ' (' . __('corresponds to the route(s)', 'routenbuch') . ' <b>' . $geo_ref['route_first'] . '</b> ';
                if ($geo_ref['route_first'] != $geo_ref['route_last']) {
                    $feed_desc .= __('to', 'routenbuch') . ' <b>' . $geo_ref['route_last'] . '</b>';
                }
                $feed_desc .= ')<br />';
            } else {
                $feed_desc .= '<br />';
            }
        }

        $route_count = count($attr['routes']);
        if ($route_count > 0) {
            if ($route_count > 3) {
                $route_first = $attr['routes'][0]['name'];
                $route_last = $attr['routes'][$route_count - 1]['name'];

                $feed_desc .= '🛑 ';
                $feed_desc .= __('Closed routes', 'routenbuch') . ': ';
                $feed_desc .= '<b>' . $route_first . '</b> ' . __('to', 'routenbuch') . ' <b>' . $route_last . '</b><br />';
            } else {
                $feed_desc .= '🛑 ';
                $feed_desc .= __('Closed routes', 'routenbuch') . ': ';

                for ($j = 0; $j < $route_count; $j++) {
                    $route = $attr['routes'][$j];

                    if ($j > 0) {
                        $feed_desc .= ', ';
                    }
                    $feed_desc .= '🛑 <b>' . $route['name'] . '</b>';
                }
            }
        }
    } else {
        $geo_ref_count = count($attr['geo_refs']);
        for ($j = 0; $j < $geo_ref_count; $j++) {
            $geo_ref = $attr['geo_refs'][$j];

            $feed_desc .= '🏔 ' . $geo_ref['name'];
            if ($geo_ref['type'] === "Sector" &&
                strlen($geo_ref['route_first']) > 0 &&
                strlen($geo_ref['route_last']) > 0) {
                $feed_desc .= ' (' . __('corresponds to the route(s)', 'routenbuch') . ' <b>' . $geo_ref['route_first'] . '</b> ';
                if ($geo_ref['route_first'] != $geo_ref['route_last']) {
                    $feed_desc .= __('to', 'routenbuch') . ' <b>' . $geo_ref['route_last'] . '</b>';
                }
                $feed_desc .= ')<br />';
            } else {
                $feed_desc .= '<br />';
            }
        }

        $route_count = count($attr['routes']);
        if ($route_count > 0) {
            $feed_desc .= '📍 ';
            $feed_desc .= __('Opened routes', 'routenbuch') . ': ';

            if ($route_count > 3) {
                $route_first = $attr['routes'][0]['name'];
                $route_last = $attr['routes'][$route_count - 1]['name'];

                $feed_desc .= '<b>' . $route_first . '</b> ' . __('to', 'routenbuch') . ' <b>' . $route_last . '</b>.<br />';
            } else {
                for ($j = 0; $j < $route_count; $j++) {
                    $route = $attr['routes'][$j];

                    if ($j > 0) {
                        $feed_desc .= ', ';
                    }
                    $feed_desc .= '<em>' . $route['name'] . '</em>';
                }
            }
        }
    }
?>
        <item>
            <title><?php echo $feed_title; ?></title>
            <link><?php echo $feed_guid_url ?></link>
            <pubDate><?php echo date('D, d M Y H:i:s +0000', $feed_time); ?></pubDate>
            <guid isPermaLink="false"><?php echo htmlspecialchars($feed_guid_url); ?></guid>
            <description><![CDATA[<?php echo $feed_desc; ?>]]></description>
        </item>
<?php
} /* end for */
?>

        <!-- End item loop -->
    </channel>
</rss>
