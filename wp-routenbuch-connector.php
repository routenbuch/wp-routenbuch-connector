<?php
/*
Plugin Name: Routenbuch Connector
Plugin URI: https://gitlab.com/routenbuch/wp-routenbuch-connector
Description: The Routenbuch Connector Plugin for Wordpress
Version: 0.1
Author: Andreas Schneider <asn@cryptomilk.org>
Author URI: https://ig-klettern.org
License: GPLv3+
*/

define('ROUTENBUCH_ASSETS_URL', plugin_dir_url( __FILE__ ) . 'assets');
define('ROUTENBUCH_TEMPLATES', plugin_dir_path( __FILE__ ) . 'templates');
define('ROUTENBUCH_LANGUAGES', plugin_dir_path( __FILE__ ) . 'languages');

class Routenbuch {
    private $options = array();

    public function __construct()
    {
        if (is_admin()) {
            $this->adminPage();
        }

        $this->add_shortcodes();
        $this->add_styles();
        $this->add_feeds();
        $this->add_languages();
    }

    private function add_languages()
    {
        add_action('plugins_loaded', array($this, 'load_i18n'));
    }

    public function load_i18n()
    {
        load_plugin_textdomain('routenbuch',
                               false, /* deprecated */
                               dirname(plugin_basename( __FILE__ )) . '/languages/');
    }

    private function add_shortcodes()
    {
        if (!class_exists('RoutenbuchShortcodes')) {
            include_once('inc/shortcodes.php');
            $r = new RoutenbuchShortcodes();
        }
    }

    public function enqueue_style()
    {
        wp_enqueue_style('routenbuch',
            ROUTENBUCH_ASSETS_URL . '/css/routenbuch.css',
            array(),
            '1.0',
            'screen');
    }

    private function add_styles()
    {
        add_action('wp_enqueue_scripts', array($this, 'enqueue_style'));
    }

    private function add_feeds()
    {
        if (!class_exists('RoutenbuchFeed')) {
            include_once('inc/feeds.php');
            $r = new RoutenbuchFeed();
        }
    }


    /*********************************************************
     * ADMIN PAGE
     *********************************************************/

    private function adminPage()
    {
        if (!class_exists('RoutenbuchAdmin')) {
            include_once('inc/admin.php');
            $r = new RoutenbuchAdmin();
            $r->add_admin_page();
        }
    }
}

/*
 * Plugin activation/deactivation
 */
function routenbuch_plugin_activate()
{
}
/* What to do when the plugin is activated? */
register_activation_hook(__FILE__, 'routenbuch_plugin_activate');

function routenbuch_plugin_deactivate()
{
    if (shortcode_exists('closure_list')) {
        remove_shortcode('closure_list');
    }
}
/* What to do when the plugin is deactivated? */
register_deactivation_hook(__FILE__, 'routenbuch_plugin_deactivate');

$GLOBALS["routenbuch"] = new Routenbuch();

?>
