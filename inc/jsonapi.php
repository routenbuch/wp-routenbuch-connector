<?php
/*
 * Copyright (c) 2021      Andreas Schneider <asn@cryptomilk.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class RoutenbuchJsonApi {
    private $options = array();

    public function __construct()
    {
        $this->options = get_option('routenbuch_options');
    }

    private function http_remote_get(String $request_url)
    {
        $username = $this->options['routenbuch_api_user'];
        $password = $this->options['routenbuch_api_pass'];

        $request_args = array(
            'headers' => array(
                'Authorization' => 'Basic ' . base64_encode($username . ':' . $password)
            )
        );

        $response = wp_remote_get($request_url, $request_args);
        $http_code = wp_remote_retrieve_response_code($response);
        if ($http_code != 200) {
            return '';
        }

        $body = wp_remote_retrieve_body($response);

        return $body;
    }

    private function http_remote_fetch(String $request_url)
    {
        $next_url = '';
        $data_count = 0;
        $data = array();

        $data_raw = $this->http_remote_get($request_url);
        if (strlen($data_raw) == 0) {
            return array();
        }

        $data = json_decode($data_raw, true /* array */);
        if (!$data) {
            return array();
        }

        if (array_key_exists('links', $data) &&
            array_key_exists('next', $data['links'])) {
            $next_url = $data['links']['next'];
        }
        unset($data['links']);

        while(strlen($next_url) > 0) {
            $data_raw = $this->http_remote_get($next_url);
            if (strlen($data_raw) == 0) {
                break;
            }

            $chunk = json_decode($data_raw, true /* array */);
            if (array_key_exists('links', $chunk) &&
                array_key_exists('next', $chunk['links'])) {
                $next_url = $chunk['links']['next'];
            } else {
                $next_url = '';
            }

            $tmp_data = array_merge($data['data'], $chunk['data']);
            if (count($tmp_data) == 0) {
                break;
            }
            $data['data'] = $tmp_data;
        }

        $data_count = count($data['data']);
        if ($data_count != $data['meta']['total']) {
            return array();
        }

        $data['meta']['last_update'] = time();
        $data['meta']['expires'] = time() + 600; /* cache expires after 10min */

        return $data;
    }

    public function fetch_data(String $request_url)
    {
        return $this->http_remote_fetch($request_url);
    }
}
