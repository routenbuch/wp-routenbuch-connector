<?php
/*
 * Copyright (c) 2021      Andreas Schneider <asn@cryptomilk.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class RoutenbuchAdmin extends Routenbuch {
    public function input_string_field($args)
    {
        $options = get_option('routenbuch_options')
        ?>
            <input name="routenbuch_options[<?php echo esc_attr($args['label_for']); ?>]" id="<?php echo esc_attr($args['label_for']); ?>" class="regular-text" type="text" value="<?php echo $options[esc_attr($args['label_for'])]; ?>" />
            <p class="description"><?php echo esc_attr($args['description']); ?></p>
        <?php
    }

    public function admin_settings_section_callback()
    {
    }

    public function register_admin_options()
    {
        register_setting('routenbuch_admin_settings_group', /* option group */
                         'routenbuch_options'); /* option name */

        add_settings_section('routenbuch_settings_section_a', /* id */
                             'REST API', /* title */
                             array($this, 'admin_settings_section_callback'), /* callback */
                             'routenbuch_admin_options_page'); /* page */

        add_settings_field('routenbuch_url', /* id */
                           'Routenbuch URL', /* title */
                           array($this, 'input_string_field'), /* callback */
                           'routenbuch_admin_options_page', /* page */
                           'routenbuch_settings_section_a', /* section */
                           array('label_for' => 'routenbuch_url',
                                 'description' => 'The URL to the Routenbuch instance (https://example.com).'));

        add_settings_field('routenbuch_api_user', /* id */
                           'Routenbuch API Username', /* title */
                           array($this, 'input_string_field'), /* callback */
                           'routenbuch_admin_options_page', /* page */
                           'routenbuch_settings_section_a', /* section */
                            array('label_for' => 'routenbuch_api_user',
                                  'description' => 'The API username to access the Routenbuch REST API.'));

        add_settings_field('routenbuch_api_pass', /* id */
                           'Routenbuch API Password', /* title */
                           array($this, 'input_string_field'), /* callback */
                           'routenbuch_admin_options_page', /* page */
                           'routenbuch_settings_section_a', /* section */
                            array('label_for' => 'routenbuch_api_pass',
                                  'description' => 'The API password to access the Routenbuch REST API.'));

        add_settings_field('routenbuch_list_page', /* id */
                           'Routenbuch closure list page', /* title */
                           array($this, 'input_string_field'), /* callback */
                           'routenbuch_admin_options_page', /* page */
                           'routenbuch_settings_section_a', /* section */
                            array('label_for' => 'routenbuch_list_page',
                                  'description' => 'The page where to find the [closure_feed] (without site url, e.g. /closure/list)'));
    }

    public function admin_options_page()
    {
        ?>
        <div class="wrap">
            <h2>Routenbuch Connector</h2>
            <form method="post" action="options.php">
                <?php
                settings_fields('routenbuch_admin_settings_group');
                do_settings_sections('routenbuch_admin_options_page');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }


    public function register_admin_menu()
    {
        add_options_page('Routenbuch Connector', /* page_title */
                         'Routenbuch', /* menu_title */
                         'manage_options', /* capability */
                         'routenbuch', /* slug */
                         array($this, 'admin_options_page')); /* function */
        add_action('admin_init', array($this, 'register_admin_options'));
    }

    public function add_admin_page()
    {
        add_action('admin_menu', array($this, 'register_admin_menu'));
    }
}
?>
