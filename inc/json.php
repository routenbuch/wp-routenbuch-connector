<?php
/*
 * Copyright (c) 2021      Andreas Schneider <asn@cryptomilk.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class RoutenbuchJSON {
    private $options = array();

    public function __construct()
    {
        $this->options = get_option('routenbuch_options');
    }

    private function get_closure_list_url($geo_ref)
    {
        $base_url = $this->options['routenbuch_url'];

        $url = $base_url;
        if ($geo_ref > 0) {
            $url .= '/api/embed/v1/geo_refs/';
            $url .= $geo_ref;
            $url .= '/closures';
        } else {
            $url .= '/api/embed/v1/closures';
        }

        $url = esc_url($url);

        return $url;
    }

    private function get_closure_feed_url($geo_ref)
    {
        $base_url = $this->options['routenbuch_url'];

        $url = $base_url;
        if ($geo_ref > 0) {
            $url .= '/api/embed/v1/geo_refs/';
            $url .= $geo_ref;
            $url .= '/closures/feed';
        } else {
            $url .= '/api/embed/v1/closures/feed';
        }

        $url = esc_url($url);

        return $url;
    }

    private function is_json_valid(Array $data)
    {
        if (!array_key_exists('data', $data) ||
            !array_key_exists('meta', $data)) {
            return false;
        }

        if (!array_key_exists('total', $data['meta']) ||
            !array_key_exists('expires', $data['meta'])) {
            return false;
        }
        $total = $data['meta']['total'];
        if ($total <= 0) {
            return false;
        }

        if (!array_key_exists('expires', $data['meta'])) {
            return false;
        }

        return true;
    }

    private function cache_expired(Array $data)
    {
        $expires = $data['meta']['expires'];
        if ($expires < time()) {
            return true;
        }

        return false;
    }

    public function get_closure_list(int $geo_ref)
    {
        if ($geo_ref < 0) {
            return null;
        }
        $content = '';
        $cached_data = array();
        $data = array();
        $update_data = false;
        $cache_valid = false;
        $cache_key = 'closure_list_geo_ref_' . $geo_ref;

        $cached_data = get_transient($cache_key);
        if ($cached_data && is_string($cached_data)) {
            $data = json_decode($cached_data, true /* array */);

            if ($this->is_json_valid($data)) {
                $cache_valid = true;
                if ($this->cache_expired($data)) {
                    $update_data = true;
                }
            } else {
                $update_data = true;
            }
        } else {
            $update_data = true;
        }

        if ($update_data) {
            $url = $this->get_closure_list_url($geo_ref);

            include_once('jsonapi.php');
            $j = new RoutenbuchJsonApi();
            $data = $j->fetch_data($url);
            if ($data && $this->is_json_valid($data)) {
                /* Sort data by name and region */
/* TODO Add an option for alphabetial sorting
                usort($data['data'], function ($a, $b) {
                    $x = $a['attributes']['name'];
                    $y = $b['attributes']['name'];
                    $a = $a['attributes']['region'][0];
                    $b = $b['attributes']['region'][0];

                    $cmp = strcasecmp($a, $b);
                    if ($cmp == 0) {
                        return strcasecmp($x, $y);
                    }

                    return $cmp;
                });
*/

                set_transient($cache_key, json_encode($data), 0);
            } else if ($cache_valid) {
                $data = json_decode($cached_data, true /* array */);
            }
        }

        return $data;
    }

    public function get_closure_feed(int $geo_ref)
    {
        if ($geo_ref < 0) {
            return null;
        }
        $content = '';
        $cache_data = array();
        $data = array();
        $update_data = false;
        $cache_valid = false;
        $cache_key = 'closure_feed_geo_ref_' . $geo_ref;

        $cached_data = get_transient($cache_key);
        if ($cached_data) {
            $data = json_decode($cached_data, true /* array */);

            if ($this->is_json_valid($data)) {
                $cache_valid = true;
                if ($this->cache_expired($data)) {
                    $update_data = true;
                }
            } else {
                $update_data = true;
            }
        } else {
            $update_data = true;
        }

        if ($update_data) {
            $url = $this->get_closure_feed_url($geo_ref);

            include_once('jsonapi.php');
            $j = new RoutenbuchJsonApi();
            $data = $j->fetch_data($url);
            if ($data && $this->is_json_valid($data)) {
                set_transient($cache_key, json_encode($data), 0);
            } else if ($cache_valid) {
                $data = $cached_data;
            }
        }

        if (!$this->is_json_valid($data)) {
            return NULL;
        }

        return $data;
    }
}
