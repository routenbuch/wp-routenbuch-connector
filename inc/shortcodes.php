<?php
/*
 * Copyright (c) 2021      Andreas Schneider <asn@cryptomilk.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class RoutenbuchShortcodes {
    private $json;
    private $options = array();

    public function __construct()
    {
        $this->options = get_option('routenbuch_options');

        if (!class_exists('RoutenbuchJSON')) {
            include_once('json.php');
        }
        $this->json = new RoutenbuchJSON();

        if (!shortcode_exists('closure_list')) {
            add_shortcode('closure_list',
                          array($this, 'shortcode_closure_list'));
        }

        if (!shortcode_exists('closure_feed')) {
            add_shortcode('closure_feed',
                          array($this, 'shortcode_closure_feed'));
        }
    }

    private function generate_closure_feed_divi(Array $data, Bool $dark = true)
    {
        $content = '';
        $link_url = get_site_url() . $this->options['routenbuch_list_page'];

        $list_count = $data['meta']['total'];
        $list = $data['data'];

        $count = min(12, $list_count);

        if ($dark) {
            $font_color = '#484848';
        } else {
            $font_color = '#ffffff';
        }

        for ($i = 0; $i < $count; $i++) {
            $element = $list[$i];
            $attr = $element['attributes'];
            $guid = hash('sha1', $attr['name'] . $attr['region'][0] . $attr['start_at'] . $attr['end_at']);

            $attr_reason = $attr['reason'];
            if ($attr['reason'] == 'breeding birds') {
                $attr_reason = __('breeding birds closure', 'routenbuch');
            } else if ($attr['reason'] == 'bats') {
                $attr_reason = __('bats closure', 'routenbuch');
            } else if ($attr['reason'] == 'protected plants') {
                $attr_reason = __('protected plants closure', 'routenbuch');
            } else if ($attr['reason'] == 'administrative closure') {
                $attr_reason = __('administrative closure', 'routenbuch');
            } else if ($attr['reason'] == 'legal regulations') {
                $attr_reason = __('legal regulations closure', 'routenbuch');
            }

            if ($attr['active']) {
                $content .= '<div class="et_pb_module et_pb_text_align_left et_pb_blurb_position_left et_pb_bg_layout_dark">';
                $content .= '<div class="et_pb_text_inner">';
                $content .= '<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-animated closure-active">Q</span></span></div>';
                $content .= '<div class="et_pb_blurb_container">';

                $content .= '<span><a style="color: ' . $font_color . ';" href="' . $link_url . '#' . $guid . '">' . $attr['name'] . ' (' . $attr['region'][0] . ') - ' . $attr_reason . ' ' . __('from', 'routenbuch') . ' ' . $attr['start_at'] . ' ' . __('to', 'routenbuch') . ' ' . $attr['end_at'] .  '.</a></span>';
            } else {
                $content .= '<div class="et_pb_module et_pb_text_align_left et_pb_blurb_position_left et_pb_bg_layout_dark">';
                $content .= '<div class="et_pb_text_inner">';
                $content .= '<div class="et_pb_main_blurb_image"><span class="et_pb_image_wrap"><span class="et-waypoint et_pb_animation_top et-pb-icon et-animated closure-inactive">R</span></span></div>';
                $content .= '<div class="et_pb_blurb_container">';

                $content .= '<span><a style="color: ' . $font_color . ';" href="' . $link_url . '#' . $guid . '">' . $attr['name'] .  ' (' . $attr['region'][0] . ') - ' . $attr_reason . ' ' . __('was lifted', 'routenbuch') . '.</a><span>';
            }
            $content .= '</div>';
            $content .= '</div>';
            $content .= '</div>';
        }

        return $content;
    }

    private function generate_closure_feed_list(Array $data, Bool $dark = true)
    {
        $content = '';
        $link_url = get_site_url() . $this->options['routenbuch_list_page'];

        $list_count = $data['meta']['total'];
        $list = $data['data'];

        $count = min(12, $list_count);

        if ($dark) {
            $font_color = '#484848';
        } else {
            $font_color = '#ffffff';
        }

        $content .= '<ul class="closure-feed">';
        for ($i = 0; $i < $count; $i++) {
            $element = $list[$i];
            $attr = $element['attributes'];
            $guid = hash('sha1', $attr['name'] . $attr['region'][0] . $attr['start_at'] . $attr['end_at']);

            $attr_reason = $attr['reason'];
            if ($attr['reason'] == 'breeding birds') {
                $attr_reason = __('breeding birds closure', 'routenbuch');
            } else if ($attr['reason'] == 'bats') {
                $attr_reason = __('bats closure', 'routenbuch');
            } else if ($attr['reason'] == 'protected plants') {
                $attr_reason = __('protected plants closure', 'routenbuch');
            } else if ($attr['reason'] == 'administrative closure') {
                $attr_reason = __('administrative closure', 'routenbuch');
            } else if ($attr['reason'] == 'legal regulations') {
                $attr_reason = __('legal regulations closure', 'routenbuch');
            }

            if ($attr['active']) {
                $content .= '<li class="closure-active"><a style="color: ' . $font_color . ';" href="' . $link_url . '#' . $guid . '">' . $attr['name'] . ' (' . $attr['region'][0] . ') - ' . $attr_reason . ' ' .  __('from', 'routenbuch')  . ' ' . $attr['start_at'] . ' ' . __('to', 'routenbuch') . ' ' . $attr['end_at'] .  '.</a></li>';
            } else {
                $content .= '<li class="closure-inactive"><a style="color: ' . $font_color . ';" href="' . $link_url . '#' . $guid . '">' . $attr['name'] .  ' (' . $attr['region'][0] . ') - ' . $attr_reason . ' ' . __('was lifted', 'routenbuch') . '.</a></li>';
            }
        }
        $content .= '</ul>';

        return $content;
    }

    private function generate_closure_list(Array $data)
    {
        $content = '';

        $list_count = $data['meta']['total'];
        $list = $data['data'];

        $current_region = '';
        $closure_active_count = 0;

        $content .= '<ul class="closure-list">';

        for ($i = 0; $i < $list_count; $i++) {
            $element = $list[$i];
            $attr = $element['attributes'];

            if (!isset($attr['start_at'])) {
                continue;
            }

            $guid = hash('sha1', $attr['name'] . $attr['region'][0] . $attr['start_at'] . $attr['end_at']);

            $attr_kind = $attr['kind'];
            if ($attr['kind'] == 'fixed') {
                $attr_kind = __('fixed', 'routenbuch');
            } else if ($attr['kind'] == 'flexible') {
                $attr_kind = __('flexible', 'routenbuch');
            } else if ($attr['kind'] == 'temporary') {
                $attr_kind = __('temporary', 'routenbuch');
            }

            $attr_reason = $attr['reason'];
            if ($attr['reason'] == 'breeding birds') {
                $attr_reason = __('Closure for', 'routenbuch') . ' ' . __('breeding birds', 'routenbuch');
            } else if ($attr['reason'] == 'bats') {
                $attr_reason = __('Closure for', 'routenbuch') . ' ' . __('bats', 'routenbuch');
            } else if ($attr['reason'] == 'protected plants') {
                $attr_reason = __('Closure for', 'routenbuch') . ' ' . __('protected plants', 'routenbuch');
            } else if ($attr['reason'] == 'administrative closure') {
                $attr_reason = __('administrative closure', 'routenbuch');
            } else if ($attr['reason'] == 'legal regulations') {
                $attr_reason = __('legal regulations', 'routenbuch');
            }

            $region = $attr['region'][0];

            if ($region != $current_region) {
                $current_region = $region;
                $content .= '<li><div class="closure-header">' . $region . '</div></li>';
            }

            /*
             *  This provides an anchor we can link to. Use sha1 as there are
             *  CPU extensions available.
             */
            $content .= '<li id="' . $guid . '">';
            $content .= '  <div class="closure-date">';
            $content .= '    ' . __('from', 'routenbuch') . ' ' . $attr['start_at'] . ' ' . __('to', 'routenbuch') .  ' ' . $attr['end_at'];
            $content .= '  </div>';
            $content .= '  <div class=closure-title">';

            $content .= '    <span>';
            if ($attr['active']) {
                $content .= '<i class="fas fa-lock closure-active"> </i> ';
            } else {
                $content .= '<i class="fas fa-lock-open closure-inactive"> </i> ';
            }
            $content .= $attr['name'] . '</span>';

            $content .= '     | <span style="font-size: smaller;">' . $attr_reason . ' - ' . $attr_kind . '</span>';
            $content .= '  </div>';
            $content .= '  <div class="closure-description">';

            if (strlen($attr['season_description']) > 0) {
                $content .= '<i class="fas fa-bell"></i> ' . $attr['season_description'] . '<br />';
            }

            if (strlen($attr['description']) > 0) {
                $content .= '<i class="fas fa-pencil-alt"></i> ' . $attr['description'] . '<br />';
            }

            if ($attr['active']) {
                $closure_active_count++;

                $geo_ref_count = count($attr['geo_refs']);
                for ($j = 0; $j < $geo_ref_count; $j++) {
                    $geo_ref = $attr['geo_refs'][$j];

                    $content .= '<i class="fas fa-map-marker-alt"></i> ' . $geo_ref['name'];
                    if ($geo_ref['type'] === "Sector" &&
                        strlen($geo_ref['route_first']) > 0 &&
                        strlen($geo_ref['route_last']) > 0) {
                        $content .= ' (' . __('corresponds to the route(s)', 'routenbuch') . ' ';
                        $content .= '<b>' . $geo_ref['route_first'] . '</b>';
                        if ($geo_ref['route_first'] != $geo_ref['route_last']) {
                            $content .= ' ' . __('to', 'routenbuch') . ' <b>' . $geo_ref['route_last'] . '</b>';
                        }
                        $content .= ')<br />';
                    } else {
                        $content .= '<br />';
                    }
                }

                $route_count = count($attr['routes']);
                if ($route_count > 0) {
                    if ($route_count > 3) {
                        $route_first = $attr['routes'][0]['name'];
                        $route_last = $attr['routes'][$route_count - 1]['name'];

                        $content .= '<i class="fas fa-directions"></i> ';
                        $content .= __('The routes', 'routenbuch') . ' <b>' . $route_first . '</b> ' . __('to', 'routenbuch') . ' <b>' . $route_last . '</b> ' . __('are closed', 'routenbuch') . '!<br />';
                    } else {
                        $content .= '<i class="fas fa-directions"></i> ';
                        $content .= __('The routes', 'routenbuch') . ': ';

                        for ($j = 0; $j < $route_count; $j++) {
                            $route = $attr['routes'][$j];

                            if ($j > 0) {
                                $content .= ', ';
                            }
                            $content .= '<b>' . $route['name'] . '</b>';
                        }
                        $content .= ' ' . __('are closed', 'routenbuch') . '!<br />';
                    }
                }
            } else {
                $geo_ref_count = count($attr['geo_refs']);
                for ($j = 0; $j < $geo_ref_count; $j++) {
                    $geo_ref = $attr['geo_refs'][$j];

                    $content .= '<i class="fas fa-map-marker-alt"></i> ' . $geo_ref['name'];
                    if ($geo_ref['type'] === "Sector" &&
                        strlen($geo_ref['route_first']) > 0 &&
                        strlen($geo_ref['route_last']) > 0) {
                        $content .= ' (' . __('from', 'routenbuch') . ' <b>' . $geo_ref['route_first'] . '</b> ' . __('to', 'routenbuch') . ' <b>' . $geo_ref['route_last'] . '</b>)<br />';
                    } else {
                        $content .= '<br />';
                    }
                }

                $route_count = count($attr['routes']);
                if ($route_count > 0) {
                    $content .= '<i class="fas fa-directions"></i> ';
                    $content .= __('Opened routes', 'routenbuch') . ': ';

                    if ($route_count > 3) {
                        $route_first = $attr['routes'][0]['name'];
                        $route_last = $attr['routes'][$route_count - 1]['name'];

                        $content .= ' <b>' . $route_first . '</b> ' . __('to', 'routenbuch') . ' <b>' . $route_last . '</b><br />';
                    } else {
                        for ($j = 0; $j < $route_count; $j++) {
                            $route = $attr['routes'][$j];

                            if ($j > 0) {
                                $content .= ', ';
                            }
                            $content .= '<b>' . $route['name'] . '</b>';

                        }
                    }
                }
            }

            $content .= '  </div>';
            $content .= '</li>';
        }

        $content .= '<li><strong>' . __('Closures total', 'routenbuch') . ': ' . $list_count . ', ' . __('active', 'routenbuch') . ': ' . $closure_active_count . ', ' . __('inactive', 'routenbuch') . ': ' . ($list_count - $closure_active_count) . '</strong></li>';
        $content .= '</ul>';

        return $content;
    }

    public function shortcode_closure_list($atts = array(),
                                           $content = null,
                                           $tag = '')
    {
        $atts = array_change_key_case((array)$atts, CASE_LOWER);
        $content = '';
        $update_data = false;

        $closures_atts = shortcode_atts(
            array(
                'geo_ref' => 0,
            ), $atts, $tag
        );
        $geo_ref = (int)$closures_atts['geo_ref'];

        $data = $this->json->get_closure_list($geo_ref);
        if (!$data) {
            return '<p>No data, please try refreshing the page!</p>';
        }

        return $this->generate_closure_list($data);
    }

    public function shortcode_closure_feed($atts = array(),
                                           $content = null,
                                           $tag = '')
    {
        $atts = array_change_key_case((array)$atts, CASE_LOWER);

        $closures_atts = shortcode_atts(
            array(
                'geo_ref' => 0,
                'style' => 'list-dark',
            ), $atts, $tag
        );
        $geo_ref = (int)$closures_atts['geo_ref'];
        $style = $closures_atts['style'];

        $data = $this->json->get_closure_feed($geo_ref);
        if (!$data) {
            return '<p>No data, please try refreshing the page!</p>';
        }

        $content = '';
        if ($style == 'list-dark') {
            $content = $this->generate_closure_feed_list($data, true);
        } else if ($style == 'list-bright') {
            $content = $this->generate_closure_feed_list($data, false);
        } else if ($style == 'divi-dark') {
            $content = $this->generate_closure_feed_divi($data, true);
        } else if ($style == 'divi-bright') {
            $content = $this->generate_closure_feed_divi($data, false);
        } else {
            $content = $this->generate_closure_feed_list($data);
        }

        return $content;
    }
}

?>
