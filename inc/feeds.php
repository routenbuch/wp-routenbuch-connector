<?php
/*
 * Copyright (c) 2021      Andreas Schneider <asn@cryptomilk.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class RoutenbuchFeed {
    private $json;

    public function __construct()
    {
        if (!class_exists('RoutenbuchJSON')) {
            include_once('json.php');
        }
        $this->json = new RoutenbuchJSON();

        add_action('init', array($this, 'register_feeds'));
    }

    public function generate_closure_feed()
    {
        $geo_ref = 0 /* Get the complete feed */;

        $data = $this->json->get_closure_feed($geo_ref);
        if (!$data) {
            return;
        }

        $template_args = array(
            'feed' => $data['data'],
            'feed_count' => $data['meta']['total'],
            'feed_update' => $data['meta']['last_update'],
        );

        load_template(ROUTENBUCH_TEMPLATES . '/feed-closure.php',
                      true, /* require_once */
                      $template_args);
    }

    public function register_feeds()
    {
        add_feed('closures',   array($this, 'generate_closure_feed'));
        add_feed('sperrungen', array($this, 'generate_closure_feed'));

        flush_rewrite_rules();
    }
}
